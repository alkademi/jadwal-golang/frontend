import 'package:jadwal_id/models/event.dart';

class Events {
  late List<Event> events;

  Events(this.events);

  Events.fromJson(Map<String, dynamic> json) {
    var list = json['data'] as List;
    List<Event> dataList = list.map((i) => Event.fromJson(i, false)).toList();
    events = dataList;
  }

  operator [](int i) => events[i]; // overloading [] operator
}
