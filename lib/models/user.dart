class User {
  late String username;
  late String? name;
  late String email;
  late String? token;
  late String? refresh;
  late String picture;

  User(this.username, this.name, this.email, this.token, this.refresh,
      this.picture);

  User.fromJson(Map<String, dynamic> json) {
    try {
      username = json['data']['username'];
      name = json['data']['namaLengkap'];
      email = json['data']['email'];
      token = json['accessToken'];
      refresh = json['refreshToken'];
    } on NoSuchMethodError {
      email = json['email'];
      username = json['username'];
      picture = json['picture'];
      name = '';
      token = '';
      refresh = '';
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['username'] = this.username;
    data['namaLengkap'] = this.name;
    data['email'] = this.email;

    return data;
  }
}
