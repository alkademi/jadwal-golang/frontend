class ApiResponse {
  late Object _data;
  late Object _apiStatus;

  Object get data => _data;
  set data(Object data) => _data = data;

  Object get apiStatus => _apiStatus;
  set apiStatus(Object status) => _apiStatus = status;
}
