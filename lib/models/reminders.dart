import 'package:jadwal_id/models/reminder.dart';

class Reminders {
  late List<Reminder> reminders;

  Reminders(this.reminders);

  Reminders.fromJson(Map<String, dynamic> json) {
    var list = json['reminders'] as List;
    List<Reminder> dataList = list.map((i) => Reminder.fromJson(i, false)).toList();
    reminders = dataList;
  }

  Reminders.fromJsonData(Map<String, dynamic> json) {
    var list = json['data'] as List;
    List<Reminder> dataList = list.map((i) => Reminder.fromJson(i, false)).toList();
    reminders = dataList;
  }

  insert(int index, Reminder reminder) {
    reminders.insert(index, reminder);
  }

  removeAt(int index) {
    reminders.removeAt(index);
  }

  operator [](int i) => reminders[i]; // overloading [] operator
}
