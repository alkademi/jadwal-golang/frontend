class Token {
  Token(this.token);

  late String token;

  Token.fromJson(Map<String, dynamic> json) {
    token = json['accessToken'];
  }
}
