class Reminder {
  Reminder(
    this.reminderId,
    this.eventId,
    this.title,
    this.scheduledAt
  );

  late int? reminderId;
  late int eventId;
  late String title;
  late DateTime scheduledAt;

  Reminder.fromJson(Map<String, dynamic> json, bool isNew) {
    reminderId = isNew ? json['data']['reminder_id'] : json['reminder_id'];
    eventId = isNew ? json['data']['event_id'] : json['event_id'];
    title = isNew ? json['data']['title'] : json['title'];
    scheduledAt = DateTime.parse(isNew ? json['data']['scheduled_at'] : json['scheduled_at']);
  }

  Map<String, dynamic> toJson() { // pots add reminder with param?event_id=int
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['title'] = title;
    data['scheduled_at'] = convertTime(scheduledAt);
    return data;
  }
}

String convertTime(DateTime time) {
  if (time.toUtc().toIso8601String().length != 27) {
    return time.toUtc().toIso8601String().substring(0, 23) + "000Z";
  } else {
    return time.toUtc().toIso8601String();
  }
}
