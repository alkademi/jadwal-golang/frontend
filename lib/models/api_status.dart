import 'package:flutter/material.dart';

class ApiStatus {
  String? _status;

  ApiStatus({@required String? status}) {
    this._status = status;
  }

  String get status => _status.toString();
  set status(String status) => _status = status;

  ApiStatus.fromJson(Map<String, dynamic> json) {
    try {
      _status = json['status'];
    } on NoSuchMethodError {
      _status = json['message'];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['status'] = this._status;
    return data;
  }
}
