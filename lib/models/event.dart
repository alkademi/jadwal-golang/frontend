import 'package:flutter/material.dart';
import 'package:jadwal_id/models/reminders.dart';

class Event {
  Event(
    this.eventId,
    this.eventName,
    this.description,
    this.from,
    this.to,
    this.background,
    this.isAllDay,
    // this.reminders
  );

  int eventId = 0;
  bool hasReminders = false;
  late String eventName;
  late String? description;
  late DateTime from;
  late DateTime to;
  late Color background;
  late bool isAllDay; // allDay?
  late Reminders? reminders;

  Event.fromJson(Map<String, dynamic> json, bool isNew) {
    if (isNew) {
      eventId = json['data']['event_id'];
      eventName = json['data']['title'];
      description = json['data']['description'];
      from = DateTime.parse(json['data']['scheduled_at']);
      to = DateTime.parse(json['data']['scheduled_until']);
      background = Color(json['data']['color']);
      isAllDay = json['data']['allday'] == 1 ? true : false;
    } else {
      eventId = json['event_id'];
      eventName = json['title'];
      description = json['description'];
      from = DateTime.parse(json['scheduled_at']);
      to = DateTime.parse(json['scheduled_until']);
      background = Color(json['color']);
      isAllDay = json['allday'] == 1 ? true : false;
      if (json['reminders'] != null) {
        // reminders = Reminders.fromJson(json['reminders']);
        reminders = Reminders.fromJson(json); // this should be right
        hasReminders = true;
      }
    }
  }

  Map<String, dynamic> toJson(bool isNew) {
    final Map<String, dynamic> data = Map<String, dynamic>();

    if (!isNew) {
      data['event_id'] = this.eventId;
    }
    data['title'] = this.eventName;
    data['description'] = this.description ?? '';
    data['scheduled_at'] = convertTime(this.from);
    data['scheduled_until'] = convertTime(this.to);
    data['color'] = this.background.value;
    data['allday'] = this.isAllDay == true ? 1 : 0;
    // data['reminders'] = this.reminders.toJson();
    //  // new shouldn't have reminders, and event not sent with reminders
    return data;
  }
}

String convertTime(DateTime time) {
  if (time.toUtc().toIso8601String().length != 27) {
    return time.toUtc().toIso8601String().substring(0, 23) + "000Z";
  } else {
    return time.toUtc().toIso8601String();
  }
}
