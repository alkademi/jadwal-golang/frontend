import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:jadwal_id/services/api.dart';
import 'package:jadwal_id/models/api_status.dart';
import 'package:jadwal_id/models/api_response.dart';
import 'package:flutter/material.dart';

void logout(context) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  ApiResponse _apiResponse = await postLogout(prefs.getString('refresh') ?? '');
  if ((_apiResponse.apiStatus as ApiStatus).status == 'OK') {
    prefs.remove('token');
    prefs.remove('refresh');
    prefs.remove('username');
    Navigator.pushNamedAndRemoveUntil(
            context, '/login', ModalRoute.withName('/login'))
        .then((_) => AwesomeNotifications().cancelAll());
  }
}
