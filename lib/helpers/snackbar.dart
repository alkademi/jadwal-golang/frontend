import 'package:flutter/material.dart';

void showInSnackBar(String text, BuildContext context, int miliseconds) {
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      content: Text(text),
      duration: Duration(milliseconds: miliseconds),
    ),
  );
}
