import 'package:shared_preferences/shared_preferences.dart';
import 'package:jadwal_id/models/api_response.dart';
import 'package:jadwal_id/models/api_status.dart';
import 'package:jadwal_id/models/token.dart';
import 'package:jadwal_id/services/api.dart';

Future<String> refreshToken() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String refresh = prefs.getString('refresh') ?? '';
  if (refresh != '') {
    ApiResponse _apiResponse = await postToken(refresh);
    if ((_apiResponse.apiStatus as ApiStatus).status == 'OK') {
      prefs.setString('token', (_apiResponse.data as Token).token);
      return (_apiResponse.data as Token).token;
    } else {
      return '';
    }
  }
  return '';
}
