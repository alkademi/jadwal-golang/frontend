import 'package:jadwal_id/models/event.dart';
import 'package:jadwal_id/models/events.dart';
import 'package:jadwal_id/models/reminder.dart';
import 'package:jadwal_id/constants/notifications.dart' as notf;
import 'package:awesome_notifications/awesome_notifications.dart';

void createNotification(Reminder reminder, String eventName, int minutesBefore) {
  AwesomeNotifications().createNotification( 
    content: NotificationContent(
        id: reminder.reminderId!,
        channelKey: notf.channelKey,
        title: reminder.title.isEmpty ? 'Untitled Reminder': reminder.title,
        body: 'Event $eventName starts in $minutesBefore minutes...'
    ),
    schedule: NotificationCalendar
      .fromDate(date: reminder.scheduledAt)
  );
}

void createNotifications(Events events) {
  var now = DateTime.now();
  var lastEvent = events.events.last;
  if (lastEvent.from.isBefore(now)) return;
  for (var event in events.events) {
    if (event.from.isAfter(now) && event.hasReminders) {
      for (var reminder in event.reminders!.reminders) {
        createNotification(reminder, event.eventName, 
          reminder.scheduledAt.difference(event.from).inMinutes.abs());
      }
    }
  }
}