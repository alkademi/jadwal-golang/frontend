import 'package:flutter/material.dart';
import 'package:jadwal_id/constants/notifications.dart' as notf;
import 'package:jadwal_id/landing.dart';
import 'package:jadwal_id/screens/add_event.dart';
import 'package:jadwal_id/screens/calendar.dart';
import 'package:jadwal_id/screens/log_in.dart';
import 'package:jadwal_id/screens/reminder.dart';
import 'package:jadwal_id/screens/settings/reset_password.dart';
import 'package:jadwal_id/screens/sign_up.dart';
import 'package:jadwal_id/screens/welcome.dart';
import 'package:jadwal_id/screens/settings.dart';
import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';

Future main() async {
  // Initialize Awesome Notifications
  AwesomeNotifications().initialize(
      null,
      [
        NotificationChannel(
            channelGroupKey: notf.groupKey,
            channelKey: notf.channelKey,
            channelName: notf.channelName,
            channelDescription: notf.channelDescription,
            defaultColor: notf.reminderColor,
            ledColor: notf.ledColor,
            importance: notf.reminderImportance,
            channelShowBadge: notf.showBadges)
      ],
      channelGroups: [
        NotificationChannelGroup(
            channelGroupkey: notf.groupKey, channelGroupName: notf.groupName)
      ],
      debug: true);

  // Initialize Splash Screen
  FlutterNativeSplash.removeAfter(splashScreenInit);

  runApp(const MyApp());
}

Future splashScreenInit(BuildContext? context) async {
  await Future.delayed(Duration(seconds: 3));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Jadwal.id',
      routes: {
        '/': (context) => Landing(),
        '/welcome': (context) => WelcomePage(),
        '/register': (context) => SignUp(),
        '/login': (context) => LogIn(),
        '/home': (context) => Calendar(),
        '/add-event': (context) => AddEventScreen(),
        '/reminder': (context) => ReminderScreen(),
        '/settings': (context) => SettingsScreen(),
        '/reset-password': (context) => ResetPasswordScreen(),
      },
      // theme: ThemeData(
      //   primarySwatch: Colors.blue,
      // ),
    );
  }
}
