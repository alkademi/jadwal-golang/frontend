import 'package:flutter/material.dart';
import 'package:awesome_notifications/awesome_notifications.dart';

const String groupKey =  'reminder_channel_group';
const String groupName = 'Reminder group';
const String channelKey = 'reminder_channel';
const String channelName = 'Reminder Notifications';
const String channelDescription = 'Notification channel for reminders';
const Color reminderColor = Color(0xFF9D50DD);
const Color ledColor = Colors.white;
const NotificationImportance reminderImportance = NotificationImportance.Max;
const bool showBadges = false;