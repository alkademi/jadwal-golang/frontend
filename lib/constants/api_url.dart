class ApiUrl {
  static const String root = 'http://34.143.175.140';
  static const String landing = '/';
  static const String register = '/register';
  static const String login = '/login';
  static const String logout = '/logout';
  static const String token = '/token';
  static const String homepage = '/home';
  static const String user = '/user';
  static const String users = '/users';
  static const String events = '/events';
  static const String reminders = '/reminders';
  static const String resetPassword = '/reset-password';
}
