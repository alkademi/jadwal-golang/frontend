import 'package:jadwal_id/models/api_response.dart';
import 'package:jadwal_id/constants/api_url.dart';
import 'package:jadwal_id/models/api_status.dart';
import 'package:jadwal_id/models/reminders.dart';
import 'package:jadwal_id/models/reminder.dart';
import 'package:jadwal_id/models/events.dart';
import 'package:jadwal_id/models/event.dart';
import 'package:jadwal_id/models/user.dart';
import 'package:jadwal_id/models/token.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

/// USERS

Future<ApiResponse> postLogin(String attribute, String password) async {
  ApiResponse _apiResponse = ApiResponse();

  try {
    Map data = {
      'attribute': attribute,
      'password': password,
    };
    var body = json.encode(data);

    final response =
        await http.post(Uri.parse(ApiUrl.root + ApiUrl.login), body: body);

    switch (response.statusCode) {
      case 200:
        _apiResponse.data = User.fromJson(json.decode(response.body));
        _apiResponse.apiStatus = ApiStatus.fromJson(json.decode(response.body));
        break;
      case 401:
        // wrong password
        // _apiResponse.apiStatus = ApiStatus.fromJson(json.decode(response.body));
        _apiResponse.apiStatus =
            ApiStatus(status: 'Email, username, atau kata sandi salah!');
        break;
      case 403:
        // wrong username
        // _apiResponse.apiStatus = ApiStatus.fromJson(json.decode(response.body));
        _apiResponse.apiStatus =
            ApiStatus(status: 'Email, username, atau kata sandi salah!');
        break;
      default:
        _apiResponse.apiStatus = ApiStatus.fromJson(json.decode(response.body));
        break;
    }
  } on Exception catch (SocketException) {
    _apiResponse.apiStatus =
        ApiStatus(status: "Server error. Mohon coba lagi!");
  }
  return _apiResponse;
}

// Logout
// url is root + logout
// body is refreshToken
Future<ApiResponse> postLogout(String refreshToken) async {
  ApiResponse _apiResponse = ApiResponse();

  try {
    Map data = {
      'refreshToken': refreshToken,
    };
    var body = json.encode(data);

    final response =
        await http.post(Uri.parse(ApiUrl.root + ApiUrl.logout), body: body);

    switch (response.statusCode) {
      case 200:
        _apiResponse.apiStatus = ApiStatus.fromJson(json.decode(response.body));
        break;
      default:
        _apiResponse.apiStatus = ApiStatus.fromJson(json.decode(response.body));
        break;
    }
  } on Exception catch (SocketException) {
    _apiResponse.apiStatus =
        ApiStatus(status: "Server error. Mohon coba lagi!");
  }
  return _apiResponse;
}

Future<ApiResponse> postAuthenticateUser(
    String namaLengkap, String username, String email, String password) async {
  ApiResponse _apiResponse = ApiResponse();

  try {
    Map data = {
      'namaLengkap': namaLengkap,
      'username': username,
      'email': email,
      'password': password,
    };
    var body = json.encode(data);

    final response =
        await http.post(Uri.parse(ApiUrl.root + ApiUrl.register), body: body);

    switch (response.statusCode) {
      case 200:
        _apiResponse.data = User.fromJson(json.decode(response.body));
        _apiResponse.apiStatus = ApiStatus.fromJson(json.decode(response.body));
        break;
      case 409:
        _apiResponse.apiStatus = ApiStatus(status: 'User sudah ada!');
        // _apiResponse.apiStatus = ApiStatus.fromJson(json.decode(response.body));
        break;
      default:
        _apiResponse.apiStatus = ApiStatus.fromJson(json.decode(response.body));
        break;
    }
  } on Exception catch (SocketException) {
    _apiResponse.apiStatus =
        ApiStatus(status: "Server error. Mohon coba lagi!");
  }
  return _apiResponse;
}

Future<ApiResponse> getUserDetails(String token) async {
  ApiResponse _apiResponse = ApiResponse();

  try {
    Map<String, String> requestHeaders = {
      'Authorization': 'Bearer ' + token,
    };

    final response = await http.get(Uri.parse(ApiUrl.root + ApiUrl.user),
        headers: requestHeaders);

    switch (response.statusCode) {
      case 200:
        _apiResponse.data = User.fromJson(json.decode(response.body));
        // _apiResponse.apiStatus = ApiStatus.fromJson(json.decode(response.body));
        _apiResponse.apiStatus = ApiStatus(status: 'OK');
        break;
      case 401:
        // invalid token
        _apiResponse.apiStatus = ApiStatus(status: 'Token tidak valid!');
        break;
      default:
        _apiResponse.apiStatus = ApiStatus.fromJson(json.decode(response.body));
        break;
    }
  } on Exception catch (SocketException) {
    _apiResponse.apiStatus =
        ApiStatus(status: "Server error. Mohon coba lagi!");
  }

  return _apiResponse;
}

Future<ApiResponse> postToken(String refresh) async {
  ApiResponse _apiResponse = ApiResponse();

  try {
    Map<String, String> data = {
      'refreshToken': refresh,
    };
    var body = json.encode(data);

    final response =
        await http.post(Uri.parse(ApiUrl.root + ApiUrl.token), body: body);

    switch (response.statusCode) {
      case 200:
        _apiResponse.data = Token.fromJson(json.decode(response.body));
        _apiResponse.apiStatus = ApiStatus.fromJson(json.decode(response.body));
        break;
      default:
        _apiResponse.apiStatus = ApiStatus.fromJson(json.decode(response.body));
        break;
    }
  } on Exception catch (SocketException) {
    _apiResponse.apiStatus =
        ApiStatus(status: "Server error. Mohon coba lagi!");
  }

  return _apiResponse;
}

/// EVENTS AND REMINDERS

/// [startDate] format: yyyy-MM-dd
///
/// [view] type: day, week, month
Future<ApiResponse> getEvents(
    String token, String username, String startDate, String view) async {
  ApiResponse _apiResponse = ApiResponse();

  try {
    Map<String, String> requestHeaders = {
      'Authorization': 'Bearer ' + token,
    };

    String params = "?startdate=" + startDate;
    if (view != '') {
      params += "&view=" + view;
    } else {
      params += "&view=month";
    }

    final response = await http.get(
        Uri.parse(ApiUrl.root +
            ApiUrl.users +
            "/" +
            username +
            ApiUrl.events +
            params),
        headers: requestHeaders);

    switch (response.statusCode) {
      case 200:
        _apiResponse.data = Events.fromJson(json.decode(response.body));
        _apiResponse.apiStatus = ApiStatus.fromJson(json.decode(response.body));
        break;
      case 401:
        _apiResponse.apiStatus = ApiStatus(status: 'Token tidak valid!');
        // _apiResponse.apiStatus = ApiStatus.fromJson(json.decode(response.body));
        break;
      case 404:
        _apiResponse.apiStatus =
            ApiStatus(status: 'Tidak ada kegiatan ditemukan!');
        break;
      default:
        _apiResponse.apiStatus = ApiStatus.fromJson(json.decode(response.body));
        break;
    }
  } on Exception catch (SocketException) {
    _apiResponse.apiStatus =
        ApiStatus(status: "Server error. Mohon coba lagi!");
  }
  return _apiResponse;
}

Future<ApiResponse> getEventReminders(
    String token, String username, int eventId) async {
  ApiResponse _apiResponse = ApiResponse();
  try {
    Map<String, String> requestHeaders = {
      'Authorization': 'Bearer ' + token,
    };

    String params = "?event-id=" + eventId.toString();
    final response = await http.get(
        Uri.parse(ApiUrl.root +
            ApiUrl.users +
            "/" +
            username +
            ApiUrl.reminders +
            params),
        headers: requestHeaders);

    switch (response.statusCode) {
      case 200:
        _apiResponse.data = Reminders.fromJsonData(json.decode(response.body));
        _apiResponse.apiStatus = ApiStatus.fromJson(json.decode(response.body));
        break;
      case 401:
        _apiResponse.apiStatus = ApiStatus(status: 'Token tidak valid!');
        break;
      case 404:
        _apiResponse.apiStatus =
            ApiStatus(status: 'Tidak ada reminder ditemukan!');
        break;
      default:
        _apiResponse.apiStatus = ApiStatus.fromJson(json.decode(response.body));
        break;
    }
  } on Exception catch (SocketException) {
    _apiResponse.apiStatus =
        ApiStatus(status: "Server error. Mohon coba lagi!");
  }
  return _apiResponse;
}

Future<ApiResponse> postAddEvent(
    String token, String username, Event event, bool isNew) async {
  ApiResponse _apiResponse = ApiResponse();

  try {
    Map<String, String> requestHeaders = {
      'Authorization': 'Bearer ' + token,
    };

    Map<String, dynamic> data = event.toJson(isNew);
    var body = json.encode(data);

    final response = await http.post(
        Uri.parse(ApiUrl.root + ApiUrl.users + "/" + username + ApiUrl.events),
        headers: requestHeaders,
        body: body);

    switch (response.statusCode) {
      case 200:
        _apiResponse.data = Event.fromJson(json.decode(response.body), true);
        _apiResponse.apiStatus = ApiStatus.fromJson(json.decode(response.body));
        break;
      case 401:
        _apiResponse.apiStatus = ApiStatus(status: 'Token tidak valid!');
        // _apiResponse.apiStatus = ApiStatus.fromJson(json.decode(response.body));
        break;
      case 404:
        _apiResponse.apiStatus = ApiStatus(status: 'Tidak ada kegiatan');
        // _apiResponse.apiStatus = ApiStatus.fromJson(json.decode(response.body));
        break;
      case 409:
        _apiResponse.apiStatus =
            ApiStatus(status: 'Kegiatan yang identik sudah ada!');
        // _apiResponse.apiStatus = ApiStatus.fromJson(json.decode(response.body));
        break;
      default:
        _apiResponse.apiStatus = ApiStatus.fromJson(json.decode(response.body));
        break;
    }
  } on Exception catch (SocketException) {
    _apiResponse.apiStatus =
        ApiStatus(status: "Server error. Mohon coba lagi!");
  }
  return _apiResponse;
}

Future<ApiResponse> postAddReminder(
    String token, String username, Reminder reminder, int eventID) async {
  ApiResponse _apiResponse = ApiResponse();

  try {
    Map<String, String> requestHeaders = {
      'Authorization': 'Bearer ' + token,
    };

    Map<String, dynamic> data = reminder.toJson();
    var body = json.encode(data);

    String params = "?event-id=" + eventID.toString();
    final response = await http.post(
        Uri.parse(ApiUrl.root +
            ApiUrl.users +
            "/" +
            username +
            ApiUrl.reminders +
            params),
        headers: requestHeaders,
        body: body);

    switch (response.statusCode) {
      case 200: // status OK
        _apiResponse.data = Reminder.fromJson(json.decode(response.body), true);
        _apiResponse.apiStatus = ApiStatus.fromJson(json.decode(response.body));
        break;
      case 400: // bad request, client error
        _apiResponse.apiStatus = ApiStatus(status: 'Client error');
        break;
      case 401: // invalid token
        _apiResponse.apiStatus = ApiStatus(status: 'Token tidak valid!');
        break;
      default:
        _apiResponse.apiStatus = ApiStatus.fromJson(json.decode(response.body));
        break;
    }
  } on Exception catch (SocketException) {
    _apiResponse.apiStatus =
        ApiStatus(status: "Server error. Mohon coba lagi!");
  }
  return _apiResponse;
}

Future<ApiResponse> deleteReminder(
    String token, String username, int reminderID) async {
  ApiResponse _apiResponse = ApiResponse();
  try {
    Map<String, String> requestHeaders = {
      'Authorization': 'Bearer ' + token,
    };
    String params = "?reminder-id=" + reminderID.toString();
    final response = await http.delete(
      Uri.parse(ApiUrl.root +
          ApiUrl.users +
          "/" +
          username +
          ApiUrl.reminders +
          params),
      headers: requestHeaders,
    );

    switch (response.statusCode) {
      case 200: // OK
        _apiResponse.data = Reminder.fromJson(
            json.decode(response.body), true); // can be used to undo action
        _apiResponse.apiStatus = ApiStatus.fromJson(json.decode(response.body));
        break;
      case 400: // bad request, client error
        _apiResponse.apiStatus = ApiStatus(status: 'Client error');
        break;
      case 401: // invalid token
        _apiResponse.apiStatus = ApiStatus(status: 'Token tidak valid!');
        break;
      case 404: // target not found
        _apiResponse.apiStatus = ApiStatus(status: 'Reminder tidak ditemukan');
        break;
      default:
        _apiResponse.apiStatus = ApiStatus.fromJson(json.decode(response.body));
        break;
    }
  } on Exception catch (SocketException) {
    _apiResponse.apiStatus =
        ApiStatus(status: "Server error. Mohon coba lagi!");
  }
  return _apiResponse;
}

Future<ApiResponse> deleteRemoveEvent(
    String token, String username, int? eventId) async {
  ApiResponse _apiResponse = ApiResponse();

  if (eventId != null) {
    try {
      Map<String, String> requestHeaders = {
        'Authorization': 'Bearer ' + token,
      };
      String param = "?event-id=" + eventId.toString();

      final response = await http.delete(
          Uri.parse(ApiUrl.root +
              ApiUrl.users +
              "/" +
              username +
              ApiUrl.events +
              param),
          headers: requestHeaders);

      switch (response.statusCode) {
        case 200:
          _apiResponse.data = Event.fromJson(json.decode(response.body), true);
          _apiResponse.apiStatus =
              ApiStatus.fromJson(json.decode(response.body));
          break;
        case 401:
          _apiResponse.apiStatus = ApiStatus(status: 'Token tidak valid!');
          // _apiResponse.apiStatus = ApiStatus.fromJson(json.decode(response.body));
          break;
        case 404:
          _apiResponse.apiStatus = ApiStatus(status: 'Kegiatan tidak ada');
          // _apiResponse.apiStatus = ApiStatus.fromJson(json.decode(response.body));
          break;
        default:
          _apiResponse.apiStatus =
              ApiStatus.fromJson(json.decode(response.body));
          break;
      }
    } on Exception catch (SocketException) {
      _apiResponse.apiStatus =
          ApiStatus(status: "Server error. Mohon coba lagi!");
    }

    return _apiResponse;
  } else {
    // eventId == null
    _apiResponse.apiStatus =
        ApiStatus(status: "Server error. Mohon coba lagi!");
    return _apiResponse;
  }
}

Future<ApiResponse> putEditEvent(
    String token, String username, Event event) async {
  ApiResponse _apiResponse = ApiResponse();

  try {
    Map<String, String> requestHeaders = {
      'Authorization': 'Bearer ' + token,
    };

    Map<String, dynamic> data = event.toJson(false);
    var body = json.encode(data);

    String param = "?event-id=" + event.eventId.toString();

    final response = await http.put(
        Uri.parse(ApiUrl.root +
            ApiUrl.users +
            "/" +
            username +
            ApiUrl.events +
            param),
        headers: requestHeaders,
        body: body);

    switch (response.statusCode) {
      case 200:
        _apiResponse.data = Event.fromJson(json.decode(response.body), true);
        _apiResponse.apiStatus = ApiStatus.fromJson(json.decode(response.body));
        break;
      case 401:
        // invalid token
        _apiResponse.apiStatus = ApiStatus(status: 'Token tidak valid!');
        break;
      default:
        _apiResponse.apiStatus = ApiStatus.fromJson(json.decode(response.body));
        break;
    }
  } on Exception catch (SocketException) {
    _apiResponse.apiStatus =
        ApiStatus(status: "Server error. Mohon coba lagi!");
  }
  return _apiResponse;
}

Future<ApiResponse> putEditReminder(
    String token, String username, Reminder reminder, int reminderID) async {
  ApiResponse _apiResponse = ApiResponse();

  try {
    Map<String, String> requestHeaders = {
      'Authorization': 'Bearer ' + token,
    };

    Map<String, dynamic> data = reminder.toJson();
    var body = json.encode(data);

    String params = "?reminder-id=" + reminderID.toString();
    final response = await http.put(
        Uri.parse(ApiUrl.root +
            ApiUrl.users +
            "/" +
            username +
            ApiUrl.reminders +
            params),
        headers: requestHeaders,
        body: body);

    switch (response.statusCode) {
      case 200: // status OK
        _apiResponse.apiStatus = ApiStatus.fromJson(json.decode(response.body));
        break;
      case 400: // bad request, client error
        _apiResponse.apiStatus = ApiStatus(status: 'Client error');
        break;
      case 401: // invalid token
        _apiResponse.apiStatus = ApiStatus(status: 'Token tidak valid!');
        break;
      case 404: // target not found
        _apiResponse.apiStatus = ApiStatus(status: 'Reminder tidak ditemukan');
        break;
      default:
        _apiResponse.apiStatus = ApiStatus.fromJson(json.decode(response.body));
        break;
    }
  } on Exception catch (SocketException) {
    _apiResponse.apiStatus =
        ApiStatus(status: "Server error. Mohon coba lagi!");
  }
  return _apiResponse;
}

/// SETTINGS

/// Change password
Future<ApiResponse> putChangePassword(
    String token, String username, String newPassword) async {
  ApiResponse _apiResponse = ApiResponse();

  try {
    Map<String, String> headers = {
      'Authorization': 'Bearer ' + token,
    };

    Map<String, String> data = {
      'password': newPassword,
    };
    var body = json.encode(data);

    final response = await http.put(
        Uri.parse(
            ApiUrl.root + ApiUrl.users + "/" + username + ApiUrl.resetPassword),
        headers: headers,
        body: body);

    switch (response.statusCode) {
      case 200: // OK
        _apiResponse.apiStatus = ApiStatus.fromJson(json.decode(response.body));
        break;
      case 401: // invalid token
        _apiResponse.apiStatus = ApiStatus(status: 'Token tidak valid!');
        break;
      default:
        _apiResponse.apiStatus = ApiStatus.fromJson(json.decode(response.body));
        break;
    }
  } on Exception catch (SocketException) {
    _apiResponse.apiStatus =
        ApiStatus(status: "Server error. Mohon coba lagi!");
  }
  return _apiResponse;
}
