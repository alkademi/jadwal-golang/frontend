import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:jadwal_id/services/api.dart';
import 'package:jadwal_id/models/api_status.dart';
import 'package:jadwal_id/models/api_response.dart';
import 'package:jadwal_id/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SignUp extends StatefulWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  late String _name;
  late String _username;
  late String _email;
  late String _password;

  void showInSnackBar(String value) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(value),
        duration: Duration(milliseconds: 1000),
      ),
    );
  }

  void _submit() async {
    final FormState? form = _formKey.currentState;
    if (form!.validate()) {
      form.save();
      ApiResponse _apiResponse =
          await postAuthenticateUser(_name, _username, _email, _password);
      if ((_apiResponse.apiStatus as ApiStatus).status == 'OK') {
        _saveAndRedirectToHome(_apiResponse);
      } else {
        showInSnackBar((_apiResponse.apiStatus as ApiStatus).status);
      }
    }
  }

  void _saveAndRedirectToHome(ApiResponse _apiResponse) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('token', (_apiResponse.data as User).token!);
    await prefs.setString('refresh', (_apiResponse.data as User).refresh!);
    await prefs.setString('username', (_apiResponse.data as User).username);

    String _token = prefs.getString('token') ?? '';
    ApiResponse userDetails = await getUserDetails(_token);
    await prefs.setString('email', (userDetails.data as User).email);
    await prefs.setString('picture', (userDetails.data as User).picture);

    Navigator.pushNamedAndRemoveUntil(
        context, '/home', ModalRoute.withName('/home'));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        systemOverlayStyle: SystemUiOverlayStyle.light,
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () {
            Navigator.pushNamed(context, '/');
          },
          icon: Icon(
            Icons.arrow_back_ios,
            size: 20,
            color: Colors.black,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 40),
          height: MediaQuery.of(context).size.height - 110,
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Text(
                    "Daftar",
                    style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(height: 20),
                  Text(
                    "Buat akun Jadwal.id secara gratis!",
                    style: TextStyle(
                      fontSize: 15,
                      color: Colors.grey[700],
                    ),
                  )
                ],
              ),
              Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    TextFormField(
                      key: Key('_name'),
                      decoration: const InputDecoration(
                        hintText: 'Nama lengkap',
                        prefixIcon: Icon(Icons.person),
                      ),
                      onSaved: (String? value) {
                        _name = value.toString();
                      },
                      validator: (String? value) {
                        if (value == null || value.trim().isEmpty) {
                          return 'Masukkan nama Anda!';
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 10),
                    TextFormField(
                      key: Key('_username'),
                      decoration: const InputDecoration(
                        hintText: 'Username',
                        prefixIcon: Icon(Icons.person),
                      ),
                      onSaved: (String? value) {
                        _username = value.toString();
                      },
                      validator: (String? value) {
                        RegExp alphanumeric = RegExp(r'^[a-zA-Z0-9]+$');
                        if (value == null || value.trim().isEmpty) {
                          return 'Masukkan username Anda!';
                        } else if (!alphanumeric.hasMatch(value.trim())) {
                          return 'Username hanya boleh menggunakan karakter alfanumerik!';
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 10),
                    TextFormField(
                      key: Key('_email'),
                      decoration: const InputDecoration(
                        hintText: 'E-mail',
                        prefixIcon: Icon(Icons.mail),
                      ),
                      onSaved: (String? value) {
                        _email = value.toString();
                      },
                      validator: (String? value) {
                        RegExp emailRegex = RegExp(
                            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');
                        if (value == null || value.trim().isEmpty) {
                          return 'Masukkan e-mail Anda!';
                        } else if (!emailRegex.hasMatch(value.trim())) {
                          return 'Masukkan e-mail yang valid!';
                        }
                        return null;
                      },
                      autofillHints: [AutofillHints.email],
                    ),
                    SizedBox(height: 10),
                    TextFormField(
                      key: Key('_password'),
                      obscureText: true,
                      decoration: const InputDecoration(
                        hintText: 'Kata sandi',
                        prefixIcon: Icon(Icons.lock),
                      ),
                      onSaved: (String? value) {
                        _password = value.toString();
                      },
                      validator: (String? value) {
                        if (value == null || value.trim().isEmpty) {
                          return 'Masukkan kata sandi Anda!';
                        } else if (value.length < 8) {
                          return 'Kata sandi minimal 8 karakter!';
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 30),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 5),
                      child: Container(
                        padding: EdgeInsets.only(top: 3, left: 3),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            border: Border(
                              top: BorderSide(color: Colors.black),
                              left: BorderSide(color: Colors.black),
                              bottom: BorderSide(color: Colors.black),
                              right: BorderSide(color: Colors.black),
                            )),
                        child: MaterialButton(
                          minWidth: double.infinity,
                          height: 60,
                          onPressed: () => _submit(),
                          color: Color(0xff009688),
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50),
                          ),
                          child: Text(
                            "Daftar",
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 18,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text("Sudah mempunyai akun?"),
                  InkWell(
                    child: Text(
                      " Masuk",
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 18,
                      ),
                    ),
                    onTap: () {
                      Navigator.pushNamed(context, '/login');
                    },
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
