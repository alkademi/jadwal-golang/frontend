import 'package:flutter/material.dart';
import 'package:jadwal_id/helpers/log_out.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NavBar extends StatefulWidget {
  const NavBar({Key? key}) : super(key: key);

  @override
  State<NavBar> createState() => _NavBarState();
}

class _NavBarState extends State<NavBar> {
  String _username = '';
  String _email = '';
  String _picture = '';

  @override
  void initState() {
    super.initState();
    _getUserProfile();
  }

  _getUserProfile() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();

    setState(() {
      _username = _prefs.getString('username') ?? '';
      _email = _prefs.getString('email') ?? '';
      _picture = _prefs.getString('picture') ?? '';
    });
  }

  void _handleLogout() async {
    logout(context);
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          UserAccountsDrawerHeader(
            accountName: Text(
              _username,
              style: TextStyle(fontSize: 17),
            ),
            accountEmail: Text(_email),
            currentAccountPicture: CircleAvatar(
              child: ClipOval(
                child: Image.network(
                  _picture,
                  width: 90,
                  height: 90,
                  fit: BoxFit.cover,
                  errorBuilder: (BuildContext context, Object exception,
                      StackTrace? stackTrace) {
                    return Text('Loading...');
                  },
                ),
              ),
            ),
            decoration: BoxDecoration(
              image: DecorationImage(
                image: NetworkImage(
                    'https://oflutter.com/wp-content/uploads/2021/02/profile-bg3.jpg'),
                fit: BoxFit.cover,
              ),
            ),
          ),
          ListTile(
            leading: Icon(Icons.settings),
            title: Text('Pengaturan'),
            onTap: () {
              Navigator.pushNamed(context, '/settings');
            },
          ),
          Divider(
            thickness: 1,
          ),
          ListTile(
            leading: Icon(Icons.exit_to_app),
            title: Text('Keluar'),
            onTap: _handleLogout,
          ),
        ],
      ),
    );
  }
}
