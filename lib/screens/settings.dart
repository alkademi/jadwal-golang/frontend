import 'package:flutter/material.dart';
import 'package:settings_ui/settings_ui.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({Key? key}) : super(key: key);

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  bool lockInBackground = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Pengaturan'),
          backgroundColor: Color(0xff009688),
        ),
        body: SettingsList(sections: [
          SettingsSection(
            title: Text('Akun', style: TextStyle(color: Color(0xff009688))),
            tiles: [
              SettingsTile(
                  title: Text('Ganti kata sandi'),
                  leading: Icon(Icons.lock),
                  onPressed: (BuildContext context) =>
                      Navigator.pushNamed(context, "/reset-password"))
            ],
          ),
        ]));
  }
}
