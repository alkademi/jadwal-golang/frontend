/// Reset password screen
/// uses textinputformfield to get the new password
/// then sends the new password to the server using the reset password api
/// then shows a success message
/// if the password is not valid, shows an error message
/// if the password is not changed, shows an error message
/// if the password is changed, shows a success message

import 'package:flutter/material.dart';
import 'package:jadwal_id/helpers/log_out.dart';
import 'package:jadwal_id/helpers/refresh_token.dart';
import 'package:jadwal_id/helpers/snackbar.dart';
import 'package:jadwal_id/models/api_response.dart';
import 'package:jadwal_id/models/api_status.dart';
import 'package:jadwal_id/services/api.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ResetPasswordScreen extends StatefulWidget {
  const ResetPasswordScreen({Key? key}) : super(key: key);

  @override
  _ResetPasswordScreenState createState() => _ResetPasswordScreenState();
}

class _ResetPasswordScreenState extends State<ResetPasswordScreen> {
  final _formKey = GlobalKey<FormState>();
  String _password = '';
  String _confirmPassword = '';
  final String _error = 'Kata sandi tidak sama';
  final String _success = 'Kata sandi berhasil diperbarui';

  void _setNewPassword() async {
    final FormState? form = _formKey.currentState;
    if (form!.validate()) {
      form.save();
      if (_password == _confirmPassword) {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        String _token = prefs.getString('token') ?? '';
        String _username = prefs.getString('username') ?? '';

        if (_token != '' || _username != '') {
          ApiResponse _apiResponse =
              await putChangePassword(_token, _username, _password);
          if ((_apiResponse.apiStatus as ApiStatus).status == 'OK') {
            Navigator.of(context).pop();
            showInSnackBar(_success, context, 2000);
          } else if ((_apiResponse.apiStatus as ApiStatus).status ==
              'Token tidak valid') {
            _token = await refreshToken();
            if (_token != '') {
              _apiResponse =
                  await putChangePassword(_token, _username, _password);
              if ((_apiResponse.apiStatus as ApiStatus).status == 'OK') {
                Navigator.of(context).pop();
                showInSnackBar(_success, context, 2000);
              } else {
                showInSnackBar((_apiResponse.apiStatus as ApiStatus).status,
                    context, 2000);
              }
            } else {
              logout(context);
            }
          } else {
            showInSnackBar(
                (_apiResponse.apiStatus as ApiStatus).status, context, 2000);
          }
        } else {
          logout(context);
        }
      } else {
        showInSnackBar(_error, context, 2000);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Ganti Kata Sandi'),
        backgroundColor: Color(0xff009688),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(20),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                TextFormField(
                  obscureText: true,
                  decoration: InputDecoration(
                    hintText: 'Kata sandi baru',
                    prefixIcon: Icon(Icons.lock),
                  ),
                  onSaved: (value) => _password = value.toString(),
                  validator: (String? value) {
                    if (value == null || value.trim().isEmpty) {
                      return 'Masukkan kata sandi baru!';
                    } else if (value.length < 8) {
                      return 'Kata sandi minimal 8 karakter!';
                    } else {
                      return null;
                    }
                  },
                ),
                SizedBox(height: 20),
                TextFormField(
                  obscureText: true,
                  decoration: InputDecoration(
                    hintText: 'Konfirmasi Kata sandi baru',
                    prefixIcon: Icon(Icons.lock),
                  ),
                  onSaved: (value) => _confirmPassword = value.toString(),
                  validator: (String? value) {
                    if (value == null || value.trim().isEmpty) {
                      return 'Konfirmasi kata sandi baru!';
                    } else if (value.length < 8) {
                      return 'Kata sandi minimal 8 karakter!';
                    } else {
                      return null;
                    }
                  },
                ),
                SizedBox(height: 20),
                ElevatedButton(
                  onPressed: () => _setNewPassword(),
                  style: ElevatedButton.styleFrom(
                      primary: Color(0xff009688), padding: EdgeInsets.all(10)),
                  child: Text(
                    'Simpan',
                    style: TextStyle(
                      fontSize: 16,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
