import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:jadwal_id/screens/add_event.dart';
import 'package:jadwal_id/screens/reminder.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'package:jadwal_id/helpers/snackbar.dart';
import 'package:jadwal_id/helpers/refresh_token.dart';
import 'package:jadwal_id/helpers/log_out.dart';
import 'package:jadwal_id/helpers/notifications.dart';
import 'package:jadwal_id/models/api_response.dart';
import 'package:jadwal_id/models/api_status.dart';
import 'package:jadwal_id/models/event.dart';
import 'package:jadwal_id/models/events.dart';
import 'package:jadwal_id/screens/edit_event.dart';
import 'package:jadwal_id/screens/navbar.dart';
import 'package:jadwal_id/services/api.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';

class Calendar extends StatefulWidget {
  const Calendar({Key? key}) : super(key: key);

  @override
  _CalendarState createState() => _CalendarState();
}

class _CalendarState extends State<Calendar> {
  final CalendarController _controller = CalendarController();
  final DateFormat _dateFormatDay = DateFormat("yyyy-MM-dd");
  final DateFormat _dateFormatWeek = DateFormat("yyyy-MM-dd");
  final DateFormat _dateFormatMonth = DateFormat("yyyy-MM-dd");
  String _view = '';
  String _startDateString = DateFormat("yyyy-MM-dd").format(DateTime.now());
  Events _events = Events(<Event>[]);

  late DateTime _startTime;
  late DateTime _endTime;
  late bool _isAllday;

  int? _eventId = 0;
  String? _titleText = '',
      _startTimeText = '',
      _endTimeText = '',
      _dateText = '',
      _timeDetails = '',
      _description = '';
  Color? _backgroundColor;

  _refreshEventsData(String view) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    String _token = _prefs.getString('token') ?? '';
    String _username = _prefs.getString('username') ?? '';

    if (_token != '' || _username != '') {
      ApiResponse _apiResponseEvents =
          await getEvents(_token, _username, _startDateString, view);

      if ((_apiResponseEvents.apiStatus as ApiStatus).status == 'OK') {
        _events = _apiResponseEvents.data as Events;
        createNotifications(_events); // TODO: find better solution
        return;
      } else if ((_apiResponseEvents.apiStatus as ApiStatus).status ==
          'Token tidak valid!') {
        _token = await refreshToken();
        if (_token != '') {
          _apiResponseEvents =
              await getEvents(_token, _username, _startDateString, view);
          if ((_apiResponseEvents.apiStatus as ApiStatus).status == 'OK') {
            _events = _apiResponseEvents.data as Events;
          } else {
            _events = Events(<Event>[]);
          }
        } else {
          logout(context);
        }
      } else {
        _events = Events(<Event>[]);
      }
    } else {
      logout(context);
    }
  }

  @override
  void initState() {
    super.initState();
    _startDateString = _dateFormatMonth.format(DateTime.now());
    _refreshEventsData('month');
  }

  void _viewChanged(ViewChangedDetails viewChangedDetails) async {
    SchedulerBinding.instance!.addPostFrameCallback((Duration duration) {
      // _controller.selectedDate = viewChangedDetails.visibleDates[0];
    });

    switch (_controller.view) {
      case CalendarView.day:
        _view = 'day';
        _startDateString =
            _dateFormatDay.format(viewChangedDetails.visibleDates[0]);
        break;
      case CalendarView.week:
        _view = 'week';
        _startDateString =
            _dateFormatWeek.format(viewChangedDetails.visibleDates[0]);
        break;
      case CalendarView.month:
        _view = 'month';
        _startDateString =
            _dateFormatMonth.format(viewChangedDetails.visibleDates[0]);
        break;
      default:
    }
    await _refreshEventsData(_view);
    setState(() {
      _events = _events;
    });
  }

  _setRefreshEventsData() async {
    await _refreshEventsData(_view);
    setState(() {
      _events = _events;
    });
  }

  _setRefreshEventDetails(
      Event shownDetail, int eventId, CalendarTapDetails details) async {
    int index =
        _events.events.indexWhere((element) => element.eventId == eventId);
    var show = _events.events.elementAt(index);
    var copyDetails = details;
    copyDetails.appointments![0] = show;
    Navigator.of(context).pop();
    calendarTapped(copyDetails);
  }

  Future<List<Event>> _getEventList() async {
    return _events.events;
  }

  void _remove() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    String _token = _prefs.getString('token') ?? '';
    String _username = _prefs.getString('username') ?? '';

    if (_token != '' || _username != '') {
      ApiResponse _apiResponseEvent =
          await deleteRemoveEvent(_token, _username, _eventId);

      if ((_apiResponseEvent.apiStatus as ApiStatus).status == 'OK') {
        await _setRefreshEventsData();

        Navigator.of(context).pop();
        showInSnackBar("Kegiatan berhasil dibuang", context, 1000);
        return;
      } else {
        _token = await refreshToken();
        if (_token != '') {
          _apiResponseEvent =
              await deleteRemoveEvent(_token, _username, _eventId);
          if ((_apiResponseEvent.apiStatus as ApiStatus).status == 'OK') {
            await _setRefreshEventsData();

            Navigator.of(context).pop();
            showInSnackBar("Kegiatan berhasil dibuang", context, 1000);
          } else {
            showInSnackBar((_apiResponseEvent.apiStatus as ApiStatus).status,
                context, 1000);
          }
        } else {
          logout(context);
        }
      }
    } else {
      logout(context);
    }
  }

  void _navigateEditScreen(
      Event clickedEvent, CalendarTapDetails details) async {
    try {
      final Event result = await Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => EditEventScreen(
              currentEvent: Event(_eventId!, _titleText!, _description,
                  _startTime, _endTime, _backgroundColor!, _isAllday)),
        ),
      ).then((_) async => await _setRefreshEventsData()).then((_) async =>
          await _setRefreshEventDetails(clickedEvent, _eventId!, details));
    } on TypeError {}
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      drawer: NavBar(),
      appBar: AppBar(
        title: Text("Jadwal.id"),
        centerTitle: true,
        backgroundColor: Color(0xff009688),
      ),
      body: FutureBuilder(
        future: _getEventList(),
        initialData: _events.events,
        builder: (BuildContext context, AsyncSnapshot<List<Event>> snapshot) {
          return SfCalendar(
            controller: _controller,
            onViewChanged: _viewChanged,
            view: CalendarView.month,
            initialDisplayDate: DateTime.now(),
            firstDayOfWeek: 1,
            allowedViews: const <CalendarView>[
              CalendarView.day,
              CalendarView.week,
              CalendarView.month,
            ],
            headerHeight: 50,
            showNavigationArrow: true,
            showCurrentTimeIndicator: true,
            showWeekNumber: true,
            weekNumberStyle: const WeekNumberStyle(
              backgroundColor: Color(0xff009688),
              textStyle: TextStyle(color: Colors.white, fontSize: 15),
            ),
            todayHighlightColor: Color(0xff009688),
            dataSource: EventDataSource(snapshot.data!),
            monthViewSettings: MonthViewSettings(
              appointmentDisplayMode: MonthAppointmentDisplayMode.indicator,
              showAgenda: true,
              agendaViewHeight: 200,
            ),
            showDatePickerButton: true,
            onTap: calendarTapped,
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => Navigator.of(context)
            .push(
              MaterialPageRoute(
                  builder: (context) => AddEventScreen(),
                  settings: RouteSettings(
                    arguments: {
                      'selectedDate': _controller.selectedDate ?? DateTime.now()
                    },
                  )),
            )
            .then((_) async => await _setRefreshEventsData()),
        backgroundColor: Color(0xff009688),
        child: const Icon(
          Icons.add_rounded,
          size: 35,
        ),
      ),
    );
  }

  void calendarTapped(CalendarTapDetails details) {
    try {
      if (details.targetElement == CalendarElement.appointment ||
          details.targetElement == CalendarElement.agenda) {
        late Event eventDetails;

        eventDetails = details.appointments![0];
        _eventId = eventDetails.eventId;
        _titleText = eventDetails.eventName;
        _dateText = DateFormat('MMMM dd, yyyy')
            .format(eventDetails.from.toLocal())
            .toString();
        _startTimeText = DateFormat('hh:mm a')
            .format(eventDetails.from.toLocal())
            .toString();
        _endTimeText =
            DateFormat('hh:mm a').format(eventDetails.to.toLocal()).toString();
        _startTime = eventDetails.from.toLocal();
        _endTime = eventDetails.to.toLocal();
        if (eventDetails.isAllDay) {
          _isAllday = true;
          _timeDetails = 'All day';
        } else {
          _isAllday = false;
          _timeDetails = '$_startTimeText - $_endTimeText';
        }
        _description = eventDetails.description;
        _backgroundColor = eventDetails.background;

        showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Container(child: Text('$_titleText')),
                content: Container(
                  height: 200,
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            '$_dateText',
                            style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 20,
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: _backgroundColor,
                            ),
                            width: 20,
                            height: 20,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      Row(
                        children: <Widget>[
                          Text(_timeDetails!,
                              style: TextStyle(
                                  fontWeight: FontWeight.w400, fontSize: 15)),
                        ],
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      Row(
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              height: 120.0,
                              child: SingleChildScrollView(
                                scrollDirection: Axis.vertical,
                                child: Text(_description!,
                                    style: TextStyle(
                                        fontWeight: FontWeight.w400,
                                        fontSize: 16),
                                    overflow: TextOverflow.fade),
                              ),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                actions: <Widget>[
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        if (_eventId != -1) ...[
                          Expanded(
                              child: Row(
                            children: [
                              IconButton(
                                onPressed: () => {
                                  _navigateEditScreen(eventDetails, details),
                                },
                                icon: Icon(
                                  Icons.edit_outlined,
                                  size: 27,
                                ),
                                padding: EdgeInsets.all(10),
                                constraints: BoxConstraints(),
                              ),
                              IconButton(
                                onPressed: () => Navigator.of(context)
                                    .push(
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              ReminderScreen(),
                                          settings: RouteSettings(
                                            arguments: {'event': eventDetails},
                                          )),
                                    )
                                    .then((_) async =>
                                        await _setRefreshEventsData()),
                                icon: Icon(
                                  Icons.notifications_none_rounded,
                                  size: 27,
                                ),
                                padding: EdgeInsets.all(10),
                                constraints: BoxConstraints(),
                              ),
                              IconButton(
                                onPressed: () => _remove(),
                                icon: Icon(
                                  Icons.delete_outlined,
                                  size: 27,
                                ),
                                color: Color.fromARGB(255, 230, 55, 55),
                                padding: EdgeInsets.all(10),
                                constraints: BoxConstraints(),
                              ),
                            ],
                          ))
                        ] else ...[
                          Spacer()
                        ],
                        TextButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text('Close')),
                      ]),
                ],
              );
            });
      }
    } on RangeError {}
  }
}

class EventDataSource extends CalendarDataSource {
  EventDataSource(List<Event> source) {
    appointments = source;
  }

  // TODO: get other properties

  @override
  int getId(int index) {
    return appointments![index].eventId;
  }

  @override
  String getSubject(int index) {
    return appointments![index].eventName;
  }

  @override
  DateTime getStartTime(int index) {
    return appointments![index].from.toLocal();
  }

  @override
  DateTime getEndTime(int index) {
    return appointments![index].to.toLocal();
  }

  @override
  Color getColor(int index) {
    return appointments![index].background;
  }

  @override
  bool isAllDay(int index) {
    return appointments![index].isAllDay;
  }
}
