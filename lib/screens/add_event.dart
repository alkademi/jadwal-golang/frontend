import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:jadwal_id/helpers/refresh_token.dart';
import 'package:jadwal_id/helpers/log_out.dart';
import 'package:jadwal_id/services/api.dart';
import 'package:jadwal_id/models/api_response.dart';
import 'package:jadwal_id/models/api_status.dart';
import 'package:jadwal_id/models/user.dart';
import 'package:jadwal_id/models/event.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';

class AddEventScreen extends StatefulWidget {
  const AddEventScreen({Key? key}) : super(key: key);

  @override
  _AddEventScreenState createState() => _AddEventScreenState();
}

class _AddEventScreenState extends State<AddEventScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String? eventTitle;
  String? eventDescription;
  DateTime startDateTime = DateTime.now();
  DateTime endDateTime = DateTime.now();
  bool allDayValue = false;
  Color eventColor = Colors.blue;
  bool dateSet = false;
  bool endDateSet = false;

  void _save(context) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    String _token = _prefs.getString('token') ?? '';
    String _username = _prefs.getString('username') ?? '';

    if (_token != '' || _username != '') {
      final FormState? form = _formKey.currentState;
      if (form!.validate()) {
        form.save();

        Event event = Event(
          0,
          eventTitle!,
          eventDescription,
          startDateTime,
          endDateTime,
          eventColor,
          allDayValue,
        );

        ApiResponse _apiResponseAddEvent =
            await postAddEvent(_token, _username, event, true);

        if ((_apiResponseAddEvent.apiStatus as ApiStatus).status == 'OK') {
          try {
            Navigator.of(context).pop();
          } on Exception {}
        } else if ((_apiResponseAddEvent.apiStatus as ApiStatus).status ==
            'Token tidak valid!') {
          _token = await refreshToken();
          if (_token != '') {
            _apiResponseAddEvent =
                await postAddEvent(_token, _username, event, true);
            try {
              Navigator.of(context).pop();
            } on Exception {}
          } else {
            try {
              Navigator.of(context).pop();
            } on Exception {}
            logout(context);
          }
        }
      }
    } else {
      logout(context);
    }
  }

  String getText(DateTime date) {
    return DateFormat('EEE, MMM d, yyyy HH:mm').format(date);
  }

  Future pickDateTime(BuildContext context, bool isStartDate) async {
    final newDate = await pickDate(context, isStartDate);
    if (newDate == null) return;

    final time = await pickTime(context);
    if (time == null) return;

    if (isStartDate == true) {
      setState(() {
        startDateTime = DateTime(
          newDate.year,
          newDate.month,
          newDate.day,
          time.hour,
          time.minute,
        );
        endDateTime = endDateSet
            ? (endDateTime.isAfter(startDateTime)
                ? endDateTime
                : startDateTime.add(Duration(hours: 1)))
            : startDateTime.add(Duration(hours: 1));
      });
    } else {
      setState(() {
        endDateTime = DateTime(
          newDate.year,
          newDate.month,
          newDate.day,
          time.hour,
          time.minute,
        );
        startDateTime = startDateTime.isBefore(endDateTime)
            ? startDateTime
            : endDateTime.subtract(Duration(hours: 1));
        endDateSet = true;
      });
    }
  }

  Future<DateTime?> pickDate(BuildContext context, bool isStartDate) async {
    // final initialDate = DateTime.now();
    final newDate = await showDatePicker(
      context: context,
      initialDate: isStartDate ? startDateTime : endDateTime,
      firstDate: DateTime(DateTime.now().year - 5),
      lastDate: DateTime(DateTime.now().year + 5),
    );
    if (newDate == null) return null;

    return newDate;
  }

  Future<TimeOfDay?> pickTime(BuildContext context) async {
    final initialTime = TimeOfDay.now();
    final newTime = await showTimePicker(
      context: context,
      initialTime: startDateTime != null
          ? TimeOfDay(hour: startDateTime.hour, minute: startDateTime.minute)
          : initialTime,
    );
    if (newTime == null) return null;
    dateSet = true;
    return newTime;
  }

  void pickColor(BuildContext context) => showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Pick a color'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              BlockPicker(
                pickerColor: eventColor,
                onColorChanged: (color) => setState(() {
                  eventColor = color;
                }),
              ),
              TextButton(
                child: Text(
                  'SELECT',
                  style: TextStyle(fontSize: 17),
                ),
                onPressed: () => Navigator.of(context).pop(),
              ),
            ],
          ),
        ),
      );

  @override
  Widget build(BuildContext context) {
    if (!dateSet) {
      final arguments = ModalRoute.of(context)!.settings.arguments as Map;
      startDateTime = arguments['selectedDate'];
      endDateTime = startDateTime.add(Duration(hours: 1));
    }
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        systemOverlayStyle: SystemUiOverlayStyle.light,
        backgroundColor: Colors.white,
        toolbarHeight: 20,
      ),
      body: SizedBox(
        width: double.infinity,
        child: Form(
          key: _formKey,
          child: ListView(
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(horizontal: 30),
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        IconButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          icon: Icon(
                            Icons.clear_rounded,
                            size: 27,
                          ),
                          padding: EdgeInsets.zero,
                          constraints: BoxConstraints(),
                        ),
                        Spacer(),
                        ElevatedButton(
                          onPressed: () => _save(context),
                          style: ElevatedButton.styleFrom(
                              primary: Color(0xff009688),
                              padding: EdgeInsets.all(5)),
                          child: Text(
                            'Save',
                            style: TextStyle(
                              fontSize: 16,
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 30),
                    TextFormField(
                      key: Key('_title'),
                      decoration: InputDecoration(
                        hintText: 'Add title',
                        border: InputBorder.none,
                      ),
                      style: TextStyle(fontSize: 24),
                      onSaved: (String? value) {
                        if (value?.trim().toString() == '') {
                          eventTitle = "(No Title)";
                        } else {
                          eventTitle = value?.trim().toString();
                        }
                      },
                    ),
                    SizedBox(height: 10),
                  ],
                ),
              ), // Event Title
              Divider(
                thickness: 1,
              ),
              SizedBox(height: 20),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 30),
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.schedule_rounded,
                          size: 27,
                        ),
                        SizedBox(width: 20),
                        InkWell(
                          key: Key('_startTime'),
                          child: Text(
                            getText(startDateTime),
                            style: TextStyle(
                              fontSize: 17,
                            ),
                          ),
                          onTap: () => pickDateTime(context, true),
                        ),
                      ],
                    ),
                    SizedBox(height: 20),
                    Row(
                      children: <Widget>[
                        SizedBox(
                          width: 47,
                        ),
                        InkWell(
                          key: Key('_endTime'),
                          child: Text(
                            getText(endDateTime),
                            style: TextStyle(
                              fontSize: 17,
                            ),
                          ),
                          onTap: () => pickDateTime(context, false),
                        ),
                      ],
                    ),
                    SizedBox(height: 12),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          children: const <Widget>[
                            SizedBox(
                              width: 47,
                            ),
                            Text(
                              "All day",
                              style: TextStyle(
                                fontSize: 17,
                              ),
                            ),
                          ],
                        ),
                        Switch(
                          value: allDayValue,
                          onChanged: (value) =>
                              setState(() => this.allDayValue = value),
                        ),
                      ],
                    ),
                  ],
                ),
              ), // Date Time
              Divider(
                thickness: 1,
              ),
              SizedBox(height: 20),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 30),
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: eventColor,
                          ),
                          width: 25,
                          height: 25,
                        ),
                        SizedBox(width: 20),
                        InkWell(
                          key: Key('_color'),
                          child: Text(
                            'Pick a color',
                            style: TextStyle(
                              fontSize: 17,
                            ),
                          ),
                          onTap: () => pickColor(context),
                        ),
                      ],
                    )
                  ],
                ),
              ), // Event Color
              SizedBox(height: 20),
              Divider(
                thickness: 1,
              ),
              SizedBox(height: 20),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 30),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      width: 25,
                      height: 25,
                      child: Icon(
                        Icons.notes,
                        size: 27,
                      ),
                    ),
                    SizedBox(width: 20),
                    Expanded(
                      child: TextFormField(
                        key: Key('_description'),
                        decoration: InputDecoration(
                          hintText: 'Add description',
                          border: InputBorder.none,
                        ),
                        style: TextStyle(fontSize: 17),
                        minLines: 1,
                        maxLines: 5,
                        keyboardType: TextInputType.multiline,
                        onSaved: (String? value) {
                          eventDescription = value?.trim().toString();
                        },
                      ),
                    ),
                  ],
                ),
              ), // Event Color
              SizedBox(height: 20),
              Divider(
                thickness: 1,
              ),
              Padding(
                  padding: EdgeInsets.only(
                      bottom: MediaQuery.of(context).viewInsets.bottom)),
            ],
          ),
        ),
      ),
    );
  }
}
