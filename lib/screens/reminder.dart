// // ignore_for_file: deprecated_member_use
import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:jadwal_id/helpers/log_out.dart';
import 'package:jadwal_id/helpers/refresh_token.dart';
import 'package:jadwal_id/models/api_response.dart';
import 'package:jadwal_id/models/api_status.dart';
import 'package:jadwal_id/models/event.dart';
import 'package:jadwal_id/models/reminders.dart';
import 'package:jadwal_id/models/reminder.dart';
import 'package:jadwal_id/services/api.dart';
import 'package:jadwal_id/helpers/snackbar.dart';
import 'package:jadwal_id/helpers/notifications.dart';

class ReminderScreen extends StatefulWidget {
  const ReminderScreen({Key? key}) : super(key: key);

  @override
  State<ReminderScreen> createState() => _ReminderScreenState();
}

class _ReminderScreenState extends State<ReminderScreen> {
  // final List<int> reminders = <int>[10, 15];
  late Event event;
  Reminders reminders = Reminders(<Reminder>[]);
  static const Icon trash = Icon(Icons.clear_rounded, size: 23);
  static const Icon edit = Icon(Icons.edit, size: 23);
  bool gotEvent = false;

  final TextEditingController reminderTitleController = TextEditingController();
  final TextEditingController reminderTimeController = TextEditingController();

  void refreshRemindersData() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    String _token = _prefs.getString('token') ?? '';
    String _username = _prefs.getString('username') ?? '';

    if (_token != '' || _username != '') {
      ApiResponse _apiResponse =
          await getEventReminders(_token, _username, event.eventId);
      if ((_apiResponse.apiStatus as ApiStatus).status == 'OK') {
        setState(() {
          reminders = _apiResponse.data as Reminders;
        });
      } else if ((_apiResponse.apiStatus as ApiStatus).status ==
          'Tidak ada reminder ditemukan!') {
        setState(() {
          reminders = Reminders(<Reminder>[]);
        });
      } else {
        _token = await refreshToken();
        if (_token != '') {
          _apiResponse =
              await getEventReminders(_token, _username, event.eventId);
          if ((_apiResponse.apiStatus as ApiStatus).status == 'OK') {
            setState(() {
              reminders = _apiResponse.data as Reminders;
            });
          } else if ((_apiResponse.apiStatus as ApiStatus).status ==
              'Tidak ada reminder ditemukan!') {
            setState(() {
              reminders = Reminders(<Reminder>[]);
            });
          }
        } else {
          logout(context);
        }
      }
    } else {
      logout(context);
    }
  }

  void addReminder(int value, String? title) async {
    // should post to api, then setState of reminders getting new reminders
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    String _token = _prefs.getString('token') ?? '';
    String _username = _prefs.getString('username') ?? '';
    late String reminderTitle;

    if (title == null) {
      reminderTitle = event.eventName;
    } else {
      reminderTitle = title;
    }

    if (_token != '' || _username != '') {
      Reminder reminder = Reminder(0, event.eventId, reminderTitle,
          event.from.subtract(Duration(minutes: value.abs())));

      ApiResponse _apiResponseAddReminder =
          await postAddReminder(_token, _username, reminder, event.eventId);

      if ((_apiResponseAddReminder.apiStatus as ApiStatus).status == 'OK') {
        Navigator.pop(context);
        refreshRemindersData();
        var received = _apiResponseAddReminder.data as Reminder;
        createNotification(
            received, event.eventName, value); // Creates Reminder notification
      } else {
        _token = await refreshToken();
        if (_token != '') {
          _apiResponseAddReminder =
              await postAddReminder(_token, _username, reminder, event.eventId);
          if ((_apiResponseAddReminder.apiStatus as ApiStatus).status == 'OK') {
            Navigator.pop(context);
            refreshRemindersData();
            var received = _apiResponseAddReminder.data as Reminder;
            createNotification(received, event.eventName,
                value); // Creates Reminder notification
          }
        } else {
          logout(context);
        }
      }
    } else {
      logout(context);
    }
  }

  void removeReminder(int reminderId) async {
    // should req delete then setState of reminders getting new reminders
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    String _token = _prefs.getString('token') ?? '';
    String _username = _prefs.getString('username') ?? '';

    ApiResponse _apiResponse =
        await deleteReminder(_token, _username, reminderId);
    if ((_apiResponse.apiStatus as ApiStatus).status == 'OK') {
      AwesomeNotifications()
          .cancel(reminderId); // Deletes scheduled reminder notification
      showInSnackBar("Reminder berhasil dihapus", context, 1000);
    } else {
      _token = await refreshToken();
      if (_token != '') {
        _apiResponse = await deleteReminder(_token, _username, reminderId);
        if ((_apiResponse.apiStatus as ApiStatus).status == 'OK') {
          AwesomeNotifications()
              .cancel(reminderId); // Deletes scheduled reminder notification
          showInSnackBar("Reminder berhasil dihapus", context, 1000);
        }
      } else {
        logout(context);
      }
    }
    refreshRemindersData();
  }

  void editReminder(int reminderId, int value, String? title) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    String _token = _prefs.getString('token') ?? '';
    String _username = _prefs.getString('username') ?? '';
    late String reminderTitle;

    if (title == null) {
      reminderTitle = event.eventName;
    } else {
      reminderTitle = title;
    }

    if (_token != '' || _username != '') {
      Reminder reminder = Reminder(0, event.eventId, reminderTitle,
          event.from.subtract(Duration(minutes: value.abs())));

      ApiResponse _apiResponseEditReminder =
          await putEditReminder(_token, _username, reminder, reminderId);

      if ((_apiResponseEditReminder.apiStatus as ApiStatus).status == 'OK') {
        Navigator.pop(context);
        refreshRemindersData();
        var received = _apiResponseEditReminder.data as Reminder;
        createNotification(
            received, event.eventName, value); // Creates Reminder notification
      } else {
        _token = await refreshToken();
        if (_token != '') {
          _apiResponseEditReminder =
              await putEditReminder(_token, _username, reminder, reminderId);
          if ((_apiResponseEditReminder.apiStatus as ApiStatus).status ==
              'OK') {
            Navigator.pop(context);
            refreshRemindersData();
            var received = _apiResponseEditReminder.data as Reminder;
            createNotification(received, event.eventName,
                value); // Creates Reminder notification
          }
        } else {
          logout(context);
        }
      }
    } else {
      logout(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    if (!gotEvent) {
      refreshRemindersData();
      event = (ModalRoute.of(context)!.settings.arguments as Map)['event'];
      gotEvent = true;
    }

    // Ask User permission for notifications
    AwesomeNotifications().isNotificationAllowed().then((isAllowed) {
      if (!isAllowed) {
        showDialog(
            // delete this
            context: context,
            builder: (context) => AlertDialog(
                  title: Text("Jadwal.id Notifications"),
                  content:
                      Text("Jadwal.id would like to send you notifications"),
                  actions: <Widget>[
                    TextButton(
                      onPressed: () => Navigator.pop(context),
                      child: Text(
                        "No", // text: Don't allow?
                        style: TextStyle(color: Colors.grey),
                      ),
                    ),
                    TextButton(
                      child: Text("Yes", // text: Allow?
                          style: TextStyle(color: Colors.teal)),
                      onPressed: () => AwesomeNotifications()
                          .requestPermissionToSendNotifications()
                          .then((_) => Navigator.pop(context)),
                    ),
                  ],
                  backgroundColor: Colors.white,
                ),
            barrierDismissible: false);
      }
    });

    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        systemOverlayStyle: SystemUiOverlayStyle.light,
        backgroundColor: Colors.white,
        toolbarHeight: 20,
      ),
      body: SizedBox(
          width: double.infinity,
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(horizontal: 30),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        IconButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          icon: Icon(
                            Icons.clear_rounded,
                            size: 27,
                          ),
                          padding: EdgeInsets.zero,
                          constraints: BoxConstraints(),
                        ),
                      ],
                    ),
                    SizedBox(height: 30),
                    Row(
                      children: const <Widget>[
                        Icon(
                          Icons.notifications_none,
                          size: 30,
                        ),
                        SizedBox(width: 20),
                        Text(
                          'Reminders',
                          style: TextStyle(
                            fontSize: 25,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 10),
                  ],
                ),
              ),
              Divider(
                thickness: 1,
              ),
              SizedBox(height: 20),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 30),
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        InkWell(
                            child: Text(
                              'Add reminder',
                              style: TextStyle(
                                fontSize: 18,
                              ),
                            ),
                            onTap: () => pickReminder(context)),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(height: 30),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 30),
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        InkWell(
                          child: Text(
                            'Add custom reminder',
                            style: TextStyle(
                              fontSize: 18,
                            ),
                          ),
                          onTap: () => addCustomReminder(context),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(height: 20),
              Divider(
                thickness: 1,
              ),
              Expanded(
                  child: ListView.builder(
                padding:
                    const EdgeInsets.symmetric(horizontal: 30, vertical: 5),
                itemCount: reminders.reminders.length,
                itemBuilder: (BuildContext context, int index) {
                  return Container(
                    height: 50,
                    margin: EdgeInsets.all(2),
                    child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            '${getDifference(reminders[index].scheduledAt, event.from)} minutes before',
                            style: TextStyle(
                                fontSize: 17, fontWeight: FontWeight.normal),
                          ),
                          Row(
                            children: <Widget>[
                              IconButton(
                                onPressed: () => editReminderDialog(
                                    context,
                                    reminders[index].reminderId,
                                    reminders[index].title),
                                icon: edit,
                                padding: EdgeInsets.fromLTRB(0, 0, 15, 0),
                                constraints: BoxConstraints(),
                              ),
                              IconButton(
                                onPressed: () {
                                  removeReminder(reminders[index].reminderId);
                                },
                                icon: trash,
                                padding: EdgeInsets.zero,
                                constraints: BoxConstraints(),
                              ),
                            ],
                          )
                        ]),
                  );
                },
              )),
            ],
          )),
    );
  }

  void pickReminder(BuildContext context) => showDialog(
        context: context,
        builder: (BuildContext context) => SimpleDialog(
          children: <Widget>[
            SimpleDialogOption(
              padding: EdgeInsets.symmetric(horizontal: 24, vertical: 20),
              child: Text('10 minutes before', style: TextStyle(fontSize: 17)),
              onPressed: () => addReminder(10, null),
            ),
            SimpleDialogOption(
              padding: EdgeInsets.symmetric(horizontal: 24, vertical: 20),
              child: Text('15 minutes before', style: TextStyle(fontSize: 17)),
              onPressed: () => addReminder(15, null),
            ),
            SimpleDialogOption(
              padding: EdgeInsets.symmetric(horizontal: 24, vertical: 20),
              child: Text('30 minutes before', style: TextStyle(fontSize: 17)),
              onPressed: () => addReminder(30, null),
            ),
          ],
        ),
      );

  void addCustomReminder(BuildContext context) => showDialog(
      context: context,
      builder: (BuildContext context) => AlertDialog(
            title: Text('Custom reminder'),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                TextField(
                  controller: reminderTitleController,
                  decoration: InputDecoration(
                      hintText: 'Your reminder title',
                      hintStyle: TextStyle(fontSize: 16)),
                ),
                SizedBox(height: 15),
                TextField(
                  controller: reminderTimeController,
                  decoration: InputDecoration(
                      hintText: 'Your reminder time (minutes)',
                      hintStyle: TextStyle(fontSize: 16)),
                ),
              ],
            ),
            actions: [
              TextButton(
                child: Text(
                  'Cancel',
                  style: TextStyle(fontSize: 16),
                ),
                onPressed: () => Navigator.pop(context),
              ),
              TextButton(
                child: Text(
                  'Save',
                  style: TextStyle(fontSize: 16),
                ),
                onPressed: () {
                  addReminder(int.parse(reminderTimeController.text),
                      reminderTitleController.text);
                  reminderTitleController.clear();
                  reminderTimeController.clear();
                },
              ),
            ],
          ));

  void editReminderDialog(
          BuildContext context, int reminderID, String reminderTitle) =>
      showDialog(
          context: context,
          builder: (BuildContext context) => AlertDialog(
                title: Text('Edit reminder'),
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Align(
                        child: Container(
                            alignment: Alignment.centerLeft,
                            child: Text(reminderTitle,
                                style: TextStyle(fontSize: 16)))),
                    TextField(
                      controller: reminderTitleController,
                      decoration: InputDecoration(
                          hintText: 'Edit reminder title',
                          hintStyle: TextStyle(fontSize: 16)),
                    ),
                    SizedBox(height: 15),
                    TextField(
                      controller: reminderTimeController,
                      decoration: InputDecoration(
                          hintText: 'Edit reminder time (minutes)',
                          hintStyle: TextStyle(fontSize: 16)),
                    ),
                  ],
                ),
                actions: [
                  TextButton(
                    child: Text(
                      'Cancel',
                      style: TextStyle(fontSize: 16),
                    ),
                    onPressed: () => Navigator.pop(context),
                  ),
                  TextButton(
                    child: Text(
                      'Save',
                      style: TextStyle(fontSize: 16),
                    ),
                    onPressed: () {
                      editReminder(
                          reminderID,
                          int.parse(reminderTimeController.text),
                          reminderTitleController.text);
                      reminderTitleController.clear();
                      reminderTimeController.clear();
                    },
                  ),
                ],
              ));
}

int getDifference(DateTime less, DateTime more) {
  return less.difference(more).inMinutes.abs();
}
