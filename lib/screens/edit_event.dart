import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:jadwal_id/helpers/log_out.dart';
import 'package:jadwal_id/helpers/refresh_token.dart';
import 'package:jadwal_id/services/api.dart';
import 'package:jadwal_id/models/api_response.dart';
import 'package:jadwal_id/models/api_status.dart';
import 'package:jadwal_id/models/user.dart';
import 'package:jadwal_id/models/event.dart';
// import 'package:jadwal_id/models/reminder.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';

class EditEventScreen extends StatefulWidget {
  EditEventScreen({Key? key, required this.currentEvent}) : super(key: key);

  final Event currentEvent;

  @override
  _EditEventScreenState createState() => _EditEventScreenState();
}

class _EditEventScreenState extends State<EditEventScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  late Event _event;
  late final int _id = _event.eventId;
  late String? _title = _event.eventName;
  late String? _description = _event.description;
  late DateTime _start = _event.from;
  late DateTime _end = _event.to;
  late bool _allDay = _event.isAllDay;
  late Color _color = _event.background;

  bool dateSet = false;

  @override
  void initState() {
    _event = widget.currentEvent;
  }

  void _saveEdit(context) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    String _token = _prefs.getString('token') ?? '';
    String _username = _prefs.getString('username') ?? '';

    if (_token != '' || _username != '') {
      final FormState? form = _formKey.currentState;
      if (form!.validate()) {
        form.save();

        Event event = Event(
          _id,
          _title!,
          _description,
          _start,
          _end,
          _color,
          _allDay,
        );

        setState(() {
          _event = event;
        });

        ApiResponse _apiResponseEditEvent =
            await putEditEvent(_token, _username, event);

        if ((_apiResponseEditEvent.apiStatus as ApiStatus).status == 'OK') {
          Navigator.of(context).pop(event);
        } else {
          _token = await refreshToken();
          if (_token != '') {
            _apiResponseEditEvent =
                await putEditEvent(_token, _username, event);
            if ((_apiResponseEditEvent.apiStatus as ApiStatus).status == 'OK') {
              Navigator.of(context).pop(event);
            }
          } else {
            logout(context);
          }
        }
      }
    } else {
      logout(context);
    }
  }

  String _getText(DateTime date) {
    return DateFormat('EEE, MMM d, yyyy HH:mm').format(date);
  }

  Future _pickDateTime(BuildContext context, bool isStartDate) async {
    final newDate = await _pickDate(context, isStartDate);
    if (newDate == null) return;

    final time = await _pickTime(context);
    if (time == null) return;

    if (isStartDate == true) {
      setState(() {
        _start = DateTime(
          newDate.year,
          newDate.month,
          newDate.day,
          time.hour,
          time.minute,
        );
        _end = _end.isAfter(_start) ? _end : _start.add(Duration(hours: 1));
      });
    } else {
      setState(() {
        _end = DateTime(
          newDate.year,
          newDate.month,
          newDate.day,
          time.hour,
          time.minute,
        );
        _start = _start.isBefore(_end) ? _start :  _end.subtract(Duration(hours: 1));
      });
    }
  }

  Future<DateTime?> _pickDate(BuildContext context, bool isStartDate) async {
    // final initialDate = DateTime.now();
    final newDate = await showDatePicker(
      context: context,
      initialDate: isStartDate ? _start : _end,
      firstDate: DateTime(DateTime.now().year - 5),
      lastDate: DateTime(DateTime.now().year + 5),
    );
    if (newDate == null) return null;

    return newDate;
  }

  Future<TimeOfDay?> _pickTime(BuildContext context) async {
    final initialTime = TimeOfDay.now();
    final newTime = await showTimePicker(
      context: context,
      initialTime: _start != null
          ? TimeOfDay(hour: _start.hour, minute: _start.minute)
          : initialTime,
    );
    if (newTime == null) return null;
    dateSet = true;
    return newTime;
  }

  void _pickColor(BuildContext context) => showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: Text('Pick a color'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              BlockPicker(
                pickerColor: _color,
                onColorChanged: (color) => setState(() {
                  _color = color;
                }),
              ),
              TextButton(
                child: Text(
                  'SELECT',
                  style: TextStyle(fontSize: 17),
                ),
                onPressed: () => Navigator.of(context).pop(),
              ),
            ],
          ),
        ),
      );

  @override
  Widget build(BuildContext context) {
    if (!dateSet) {
      _start = _event.from;
      _end = _event.to;
    }
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        systemOverlayStyle: SystemUiOverlayStyle.light,
        backgroundColor: Colors.white,
        toolbarHeight: 20,
      ),
      body: SizedBox(
        width: double.infinity,
        child: Form(
          key: _formKey,
          child: ListView(
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(horizontal: 30),
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        IconButton(
                          onPressed: () {
                            Navigator.of(context).pop(_event);
                          },
                          icon: Icon(
                            Icons.clear_rounded,
                            size: 27,
                          ),
                          padding: EdgeInsets.zero,
                          constraints: BoxConstraints(),
                        ),
                        Spacer(),
                        ElevatedButton(
                          onPressed: () => _saveEdit(context),
                          style: ElevatedButton.styleFrom(
                              primary: Color(0xff009688),
                              padding: EdgeInsets.all(5)),
                          child: Text(
                            'Save',
                            style: TextStyle(
                              fontSize: 16,
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 30),
                    TextFormField(
                      key: Key('_title'),
                      initialValue: _title,
                      decoration: InputDecoration(
                        hintText: 'Add title',
                        border: InputBorder.none,
                      ),
                      style: TextStyle(fontSize: 24),
                      onSaved: (String? value) {
                        if (value?.trim().toString() == '') {
                          _title = "(No Title)";
                        } else {
                          _title = value!.trim().toString();
                        }
                      },
                    ),
                    SizedBox(height: 10),
                  ],
                ),
              ), // Event Title
              Divider(
                thickness: 1,
              ),
              SizedBox(height: 20),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 30),
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.schedule_rounded,
                          size: 27,
                        ),
                        SizedBox(width: 20),
                        InkWell(
                          key: Key('_startTime'),
                          child: Text(
                            _getText(_start),
                            style: TextStyle(
                              fontSize: 17,
                            ),
                          ),
                          onTap: () => _pickDateTime(context, true),
                        ),
                      ],
                    ),
                    SizedBox(height: 20),
                    Row(
                      children: <Widget>[
                        SizedBox(
                          width: 47,
                        ),
                        InkWell(
                          key: Key('_endTime'),
                          child: Text(
                            _getText(_end),
                            style: TextStyle(
                              fontSize: 17,
                            ),
                          ),
                          onTap: () => _pickDateTime(context, false),
                        ),
                      ],
                    ),
                    SizedBox(height: 12),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Row(
                          children: const <Widget>[
                            SizedBox(
                              width: 47,
                            ),
                            Text(
                              "All day",
                              style: TextStyle(
                                fontSize: 17,
                              ),
                            ),
                          ],
                        ),
                        Switch(
                          value: _allDay,
                          onChanged: (value) =>
                              setState(() => this._allDay = value),
                        ),
                      ],
                    ),
                  ],
                ),
              ), // Date Time
              Divider(
                thickness: 1,
              ),
              SizedBox(height: 20),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 30),
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: _color,
                          ),
                          width: 25,
                          height: 25,
                        ),
                        SizedBox(width: 20),
                        InkWell(
                          key: Key('_color'),
                          child: Text(
                            'Pick a color',
                            style: TextStyle(
                              fontSize: 17,
                            ),
                          ),
                          onTap: () => _pickColor(context),
                        ),
                      ],
                    )
                  ],
                ),
              ), // Event Color
              SizedBox(height: 20),
              Divider(
                thickness: 1,
              ),
              SizedBox(height: 20),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 30),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      width: 25,
                      height: 25,
                      child: Icon(
                        Icons.notes,
                        size: 27,
                      ),
                    ),
                    SizedBox(width: 20),
                    Expanded(
                      child: TextFormField(
                        key: Key('_description'),
                        initialValue: _description,
                        decoration: InputDecoration(
                          hintText: 'Add description',
                          border: InputBorder.none,
                        ),
                        style: TextStyle(fontSize: 17),
                        minLines: 1,
                        maxLines: 5,
                        keyboardType: TextInputType.multiline,
                        onSaved: (String? value) {
                          if (value?.trim().toString() == '') {
                            _description = "";
                          } else {
                            _description = value?.trim().toString();
                          }
                        },
                      ),
                    ),
                  ],
                ),
              ), // Event Color
              SizedBox(height: 20),
              Divider(
                thickness: 1,
              ),
              Padding(
                  padding: EdgeInsets.only(
                      bottom: MediaQuery.of(context).viewInsets.bottom)),
            ],
          ),
        ),
      ),
    );
  }
}
