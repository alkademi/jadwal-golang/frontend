import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:jadwal_id/services/api.dart';
import 'package:jadwal_id/models/api_status.dart';
import 'package:jadwal_id/models/api_response.dart';
import 'package:jadwal_id/models/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LogIn extends StatefulWidget {
  const LogIn({Key? key}) : super(key: key);

  @override
  State<LogIn> createState() => _LogInState();
}

class _LogInState extends State<LogIn> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  late String _attribute;
  late String _password;

  void showInSnackBar(String value) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(value),
        duration: Duration(milliseconds: 1000),
      ),
    );
  }

  void _submit() async {
    final FormState? form = _formKey.currentState;
    if (form!.validate()) {
      form.save();
      ApiResponse _apiResponse = await postLogin(_attribute, _password);
      if ((_apiResponse.apiStatus as ApiStatus).status == 'OK') {
        _saveAndRedirectToHome(_apiResponse);
      } else {
        showInSnackBar((_apiResponse.apiStatus as ApiStatus).status);
      }
    } else {}
  }

  void _saveAndRedirectToHome(ApiResponse _apiResponse) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('token', (_apiResponse.data as User).token!);
    await prefs.setString('username', (_apiResponse.data as User).username);
    await prefs.setString('refresh', (_apiResponse.data as User).refresh!);

    String _token = prefs.getString('token') ?? '';
    ApiResponse userDetails = await getUserDetails(_token);
    await prefs.setString('email', (userDetails.data as User).email);
    await prefs.setString('picture', (userDetails.data as User).picture);

    try {
      Navigator.pushNamedAndRemoveUntil(
          context, '/home', ModalRoute.withName('/home'));
    } on FlutterError {}
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        systemOverlayStyle: SystemUiOverlayStyle.light,
        backgroundColor: Colors.white,
        leading: IconButton(
          onPressed: () {
            Navigator.pushNamed(context, '/');
          },
          icon: Icon(
            Icons.arrow_back_ios,
            size: 20,
            color: Colors.black,
          ),
        ),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      Text(
                        "Masuk",
                        style: TextStyle(
                          fontSize: 30,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(height: 20),
                      Text(
                        "Masuk ke dalam akun Jadwal.id Anda",
                        style: TextStyle(fontSize: 15, color: Colors.grey[700]),
                      ),
                    ],
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 40),
                    child: Form(
                      key: _formKey,
                      child: Column(
                        children: <Widget>[
                          TextFormField(
                            key: Key('_attribute'),
                            decoration: const InputDecoration(
                              hintText: 'Email atau username',
                              prefixIcon: Icon(Icons.person),
                            ),
                            onSaved: (String? value) {
                              _attribute = value.toString();
                            },
                            validator: (String? value) {
                              RegExp alphanumeric = RegExp(r'^[a-zA-Z0-9]+$');
                              RegExp emailRegex = RegExp(
                                  r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');

                              if (value == null ||
                                  value.trim().isEmpty ||
                                  !(alphanumeric.hasMatch(value.trim()) ||
                                      emailRegex.hasMatch(value.trim()))) {
                                return 'Masukkan username atau email Anda!';
                              }
                              return null;
                            },
                          ),
                          SizedBox(height: 10),
                          TextFormField(
                            key: Key('_password'),
                            obscureText: true,
                            decoration: const InputDecoration(
                              hintText: 'Kata sandi',
                              prefixIcon: Icon(Icons.lock),
                            ),
                            onSaved: (String? value) {
                              _password = value.toString();
                            },
                            validator: (String? value) {
                              if (value == null || value.trim().isEmpty) {
                                return 'Kata sandi minimal 8 karakter!';
                              }
                              return null;
                            },
                          ),
                          SizedBox(height: 30),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 5),
                            child: Container(
                              padding: EdgeInsets.only(top: 3, left: 3),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(50),
                                  border: Border(
                                    top: BorderSide(color: Colors.black),
                                    left: BorderSide(color: Colors.black),
                                    bottom: BorderSide(color: Colors.black),
                                    right: BorderSide(color: Colors.black),
                                  )),
                              child: MaterialButton(
                                minWidth: double.infinity,
                                height: 60,
                                onPressed: () => _submit(),
                                color: Color(0xff009688),
                                elevation: 0,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(50),
                                ),
                                child: Text(
                                  "Masuk",
                                  style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 18,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text("Belum mempunyai akun?"),
                      InkWell(
                        child: Text(
                          " Daftar",
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 18,
                          ),
                        ),
                        onTap: () {
                          Navigator.pushNamed(context, '/register');
                        },
                      ),
                    ],
                  ),
                  Container(
                    padding: EdgeInsets.only(top: 100),
                    height: 200,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage("assets/login.png"),
                          fit: BoxFit.fitHeight),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
