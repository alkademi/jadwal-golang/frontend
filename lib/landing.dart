import 'package:flutter/material.dart';
import 'package:jadwal_id/helpers/log_out.dart';
import 'package:jadwal_id/helpers/refresh_token.dart';
import 'package:jadwal_id/models/api_response.dart';
import 'package:jadwal_id/models/api_status.dart';
import 'package:jadwal_id/models/user.dart';
import 'package:jadwal_id/services/api.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Landing extends StatefulWidget {
  const Landing({Key? key}) : super(key: key);

  @override
  _LandingState createState() => _LandingState();
}

class _LandingState extends State<Landing> {
  String _token = '';

  @override
  void initState() {
    super.initState();
    _loadUserInfo();
  }

  _loadUserInfo() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    _token = prefs.getString('token') ?? '';
    String _refresh = prefs.getString('refresh') ?? '';
    if (_refresh == '') {
      Navigator.pushNamedAndRemoveUntil(
          context, '/welcome', ModalRoute.withName('/welcome'));
    } else {
      ApiResponse _apiResponse = await getUserDetails(_token);
      if ((_apiResponse.apiStatus as ApiStatus).status == 'OK') {
        await prefs.setString('email', (_apiResponse.data as User).email);
        await prefs.setString('picture', (_apiResponse.data as User).picture);
        Navigator.pushNamedAndRemoveUntil(
            context, '/home', ModalRoute.withName('/home'));
      } else if ((_apiResponse.apiStatus as ApiStatus).status ==
          'Token tidak valid!') {
        _token = await refreshToken();
        if (_token != '') {
          await prefs.setString('email', (_apiResponse.data as User).email);
          await prefs.setString('picture', (_apiResponse.data as User).picture);
          Navigator.pushNamedAndRemoveUntil(
              context, '/home', ModalRoute.withName('/home'));
        } else {
          logout(context);
        }
      } else {
        logout(context);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: Center(child: CircularProgressIndicator()));
  }
}
