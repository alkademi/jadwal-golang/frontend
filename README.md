# IF3250_2022_33_JADWAL_FRONTEND

## How to run

### Docker

1. Install docker
2. Build docker image with command `docker build . -t flutter_docker`
3. Run docker with `docker run -i -p <port kosong>:5000 -td flutter_docker`
4. Frontend API endpoint is at `http://localhost:<port kosong>`

Run this docker with backend server at localhost:8090 already loaded, so the connection will not be refused

1. [Install Flutter](https://docs.flutter.dev/get-started/install).
2. Jalankan perintah `flutter pub get` pada root folder.
3. Buka editor pilihan.
4. Jalankan aplikasi di emulator pilihan atau menggunakan USB debugging.
